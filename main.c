#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>

typedef struct {
    int kmStart;
    int kmEnd;
    double toll;
} TOLL;
TOLL **taxA;
int arrSize=5,maxKM;

int numberOfChanges[27];

int freeArray(){
   /* int i;
    for(i=0;i<27;i++){
        free(taxA[i]);
    }
    free(taxA);*/
    return 0;
}
int badInput(){
    printf("Nespravny vstup.\n");
    freeArray();
    exit(1);
}
void nullArray(int letterNEW,int pocetNEW){
    int i;
    taxA= (TOLL **)calloc( letterNEW,letterNEW * sizeof ( TOLL *) );
    /* if ( taxA==NULL ) {
         free ( *taxA );
         printf("NO MEMORY FOR YOU!!!!\n");
         return -1;
     }*/



    for(i=0;i<letterNEW;i++) {
        taxA [i] = (TOLL *)calloc(pocetNEW, pocetNEW *sizeof(TOLL) );
        /* if ( taxA [i]==NULL ) {
             free ( taxA );
             printf("NO MEMORY FOR YOU!!!!\n");
             return -1;
         }*/

    }
}
int updateArray(int letterNEW,int pocetNEW){
    int i=0;

    taxA= (TOLL **)realloc( taxA,letterNEW * sizeof ( TOLL *) );
    if ( taxA==NULL ) {
        free ( *taxA );
        printf("NO MEMORY FOR YOU!!!!\n");
        return -1;
    }



    for(i=0;i<letterNEW;i++) {
        taxA [i] = (TOLL *)realloc( taxA[i],pocetNEW *sizeof(TOLL) );
        if ( taxA [i]==NULL ) {
            free ( taxA );
            printf("NO MEMORY FOR YOU!!!!\n");
            return -1;
        }

    }
    //nullArray(letterNEW, pocetNEW);
    return 0;
}
int createArray(int letterNEW,int pocetNEW){
    int i=0;
TOLL ** taxA;
    taxA= (TOLL **)malloc( letterNEW * sizeof ( TOLL *) );
    /* if ( taxA==NULL ) {
         free ( *taxA );
         printf("NO MEMORY FOR YOU!!!!\n");
         return -1;
     }*/



    for(i=0;i<letterNEW;i++) {
        taxA [i] = (TOLL *)malloc( pocetNEW *sizeof(TOLL) );
        /* if ( taxA [i]==NULL ) {
             free ( taxA );
             printf("NO MEMORY FOR YOU!!!!\n");
             return -1;
         }*/

    }
    nullArray(letterNEW, pocetNEW);
    return 0;
}
int updateOtherRoads(int startKM,int length){
    int i;
    for(i=0;i<27;i++){
        if(numberOfChanges[i]!=0){

            if(taxA[i][numberOfChanges[i]-1].kmEnd==startKM){
                taxA[i][numberOfChanges[i]-1].kmEnd=length;
            }
        }else{
            taxA[i][numberOfChanges[i]].kmEnd=length;
        }

    }
    return 0;
}
int pasteIntoArray(char letter,int min,int max,double tax){
    int letterInt;

    if(isupper(letter)==0) badInput();
    letterInt=letter-'A';
    if(letterInt<0||letterInt>26)badInput();
    //printf("%d\n",numberOfChanges[letterInt]);

    taxA[letterInt][numberOfChanges[letterInt]].toll=tax;
    taxA[letterInt][numberOfChanges[letterInt]].kmEnd=max;
    taxA[letterInt][numberOfChanges[letterInt]].kmStart=min;
    // printf("Tax Bracket: %c     KM Range:%d-%d     Tax Price:%lf \n",letter,taxA[letterInt][numberOfChanges[letterInt]].kmStart,taxA[letterInt][numberOfChanges[letterInt]].kmEnd,taxA[letterInt][numberOfChanges[letterInt]].toll);
    if(numberOfChanges[letterInt]>(arrSize-3)){
        arrSize=arrSize*1.5;
        updateArray(27,arrSize);

    }
    numberOfChanges[letterInt]++;
    //printf("Tax Bracket: %c     KM Range:%d-%d     Tax Price:%lf \n",letter,taxA[letterInt][numberOfChanges[letterInt]].kmStart,taxA[letterInt][numberOfChanges[letterInt]].kmEnd,taxA[letterInt][numberOfChanges[letterInt]].toll);

    //printf("%d,%d\n",numberOfChanges[letterInt],letterInt);
    //printf("%d\n",letterInt);
    return 0;
}
int getInput(){
    int isEOF=0,length,startKM=0,check;
    printf("Myto:\n");
    char checkStart,checkEnd,letter;
    double price ;
    isEOF=scanf(" %c %c",&checkStart ,&checkEnd);

    if(isEOF==EOF||checkStart!='{'||checkEnd!='['){
        return 1;
    }
    while(666){

        check=scanf(" %d : %c = %lf",&length,&letter,&price);
        if(check!=3)return 1;
        if(price<0)return 1;
        if(length<=0)return 1;
        maxKM=length=startKM+length;
        pasteIntoArray(letter,startKM,length,price);
        scanf(" ");
        checkEnd=getchar();
        if(checkEnd==',') {

            while (666) {
                check=scanf(" %c = %lf", &letter, &price);
                if(check!=2)return 1;

                pasteIntoArray(letter,startKM,length,price);


                scanf(" ");
                checkEnd=getchar();
                if(checkEnd==',') {}
                else if(checkEnd==']')break;
                else return 1;


            }
        }else if(checkEnd==']'){}
        else badInput();

        scanf(" %c",&checkEnd);
        if(checkEnd==','){
            scanf(" %c",&checkEnd);
            if(checkEnd=='['){}
            else if(checkEnd=='}'){break;}
            else {return 1;}

        }else if(checkEnd=='}'){
            break;
        }else return 1;

        updateOtherRoads(startKM,length);
        startKM=length;
    }
    updateOtherRoads(startKM,length);
    return 0;

}
void printArray(){
    int i,x;
    for(i=0;i<27;i++){
        for(x=0;x<numberOfChanges[i];x++){
            printf("Tax Bracket: %d     KM Range:%d-%d     Tax Price:%lf \n",i,taxA[i][x].kmStart,taxA[i][x].kmEnd,taxA[i][x].toll);
        }
    }
}

int getInput2(int *start,int *end){
    int isEOF;
    isEOF=scanf(" %d %d",start,end);

    if(isEOF==EOF)return EOF;
    else if (isEOF!=2) badInput();
    else if (*start==*end||*start<0||*end<0||*start>maxKM||*end>maxKM) badInput();
    printf("%d - %d:",*start,*end);

    if(*start>*end){
        int tmp;
        tmp=*start;
        *start=*end;
        *end=tmp;
    }

    return 0;


}
double getTaxes(int letter,int start,int end){
    int i,ridden,hasToRide=end-start;
    double bill=0;

    if(start<taxA[letter][0].kmStart){
        start=taxA[letter][0].kmStart;
        if(start>=end) return bill;
        hasToRide=end-start;
    }
    for(i=0;i<numberOfChanges[letter];i++){
        if(taxA[letter][i].kmStart<=start&&
           taxA[letter][i].kmEnd>start) {

            if(hasToRide>(taxA[letter][i].kmEnd-start)){
                ridden =taxA[letter][i].kmEnd-start;
                start=start+ridden;
                hasToRide=end-start;
                bill=bill+ridden*taxA[letter][i].toll;
            }else{

                ridden=hasToRide;
                bill=bill+ridden*taxA[letter][i].toll;
                break;
            }


        }else{

        }
    }
    return bill;
}
int main() {
    freopen("C:\\Users\\malyp\\CLionProjects\\DomaciUloha5_Lehci\\tests\\0007_in.txt","r",stdin);

    createArray(27,arrSize);

    //printf("%d,,,,,%d,%d,%lf\n",sizeof(TOLL**),taxA[26][1].kmEnd,taxA[0][10000].kmStart,taxA[0][55].toll);
    if(getInput()!=0){badInput();}
   // printArray();
    int isEOF=0,start,end,i;
    char letter;
    double bill;
    printf("Hledani:\n");
    while(666) {

        isEOF = getInput2(&start,&end);

        if(isEOF==EOF)break;
        int comma=0;
        for(i=0;i<27;i++){

            if(numberOfChanges[i]!=0){
                bill=0;
                bill=getTaxes(i,start,end);
                if(bill!=0&&comma==0){
                    letter='A'+i;
                    printf(" %c=%lf",letter,bill);
                    comma=1;
                }else if(bill!=0){
                    letter='A'+i;
                    printf(", %c=%lf",letter,bill);
                }


            }
        }
        printf("\n");
    }
    freeArray();
    return 0;
}
